import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth-service/auth.service';
import { finalize, tap } from 'rxjs/operators'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // subscriptions
  loginSub$: Subscription = new Subscription();

  isLoading: boolean = false;
  showPassword: boolean = false;

  // form controls
  emailFormControl: FormControl;
  passwordFormControl: FormControl;


  constructor(private authServ: AuthService, private router: Router) {
    this.emailFormControl = new FormControl(null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]);
    this.passwordFormControl = new FormControl(null, [Validators.required, Validators.minLength(6)]);
  }

  ngOnInit(): void {
  }

  // password visibility
  togglePasswordVisibility(): void {
    this.showPassword = !this.showPassword;
  }

  login() {
    this.isLoading = true;
    const loginData = {
      email: this.emailFormControl.value,
      password: this.passwordFormControl.value,
    }

    this.loginSub$ = this.authServ.login(loginData).pipe(tap((res: { token: any, user: any }) => {
      switch (res.user.role) {
        case 'superadmin':
          // navigate this user to next route
          this.router.navigate([''])
          break;
      }
    }), finalize(() => this.isLoading = false)).subscribe()

  }

  ngOnDestroy(): void {
    this.loginSub$.unsubscribe();
  }
}
