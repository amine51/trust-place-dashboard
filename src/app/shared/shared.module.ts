import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


// ng meterial modules
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';





@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    
    // ng material modules 
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatInputModule
  ],
  exports: [
    CommonModule,
    // ng material modules 
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatInputModule

  ]
})
export class SharedModule { }
