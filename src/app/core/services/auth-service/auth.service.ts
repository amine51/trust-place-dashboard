import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, tap } from 'rxjs/operators'
import { LocalStorageService } from '../local-storage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authenticatedUser$ = new BehaviorSubject<any | null>(null);

  constructor(private http: HttpClient, private localStorageServ: LocalStorageService) { }

  storeAuthData(data: { user: any, token: string }): void {
    this.localStorageServ.setItem('token', data.token);
    this.localStorageServ.setItem('user', data.user);
    this.authenticatedUser$.next((data.user));
  }

  login(logData: { email: string, password: string }): Observable<any> {
    return this.http.get<any>(environment.apiUrl).pipe(map((res: any) => res.data), tap(this.storeAuthData.bind(this)))
  }

  logout(): void {
    this.localStorageServ.clearStorage();
  }

  loadAuthData(): void {
    const authData = {
      token: this.localStorageServ.getItem('token'),
      user: this.localStorageServ.getItem('user'),
    }
    this.authenticatedUser$.next(authData.user)
  }

  set AuthenticatedUser(user: any) {
    this.authenticatedUser$.next(user);
  }

  get AuthenticatedUser(): Observable<any | null> {
    return this.authenticatedUser$.asObservable();
  }

  isAuthenticatedUser() {
    return this.authenticatedUser$ ? true : false;
  }

  getStoredUserToken(): string | null {
    return this.localStorageServ.getItem("token");
  }


}
