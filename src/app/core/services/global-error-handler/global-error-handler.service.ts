import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { AuthService } from '../auth-service/auth.service';
import Errors from './errors';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandlerService implements ErrorHandler {
  private readonly DEFAULT_HTTP_TITLE = `oups ! Une erreur serveur interne s'est produite`;

  constructor(private authServ: AuthService) { }


  public handleError(errorResponse: any) {
    const errors: any = Errors['fr'];

    // case 1 : HTTP ERROR
    if (errorResponse instanceof HttpErrorResponse) {
      if (errorResponse.error.status === 401) {
        // log user out and redirect to login page
        this.authServ.logout();
        return;
      }

      if (errorResponse.error.status === 500) {
        console.log(this.DEFAULT_HTTP_TITLE)
        return;
      }
      const message = errors[errorResponse.error.errorCode];
      if (message) {
        console.log(message);
        console.log(errorResponse);
        return;
      }
      console.log(`Une erreur HTTP TRUST-PLACE inconnue s'est produite`);
      console.log(errorResponse);
      return;
    }
    // case 2 : CLIENT SIDE ERROR (exemple TS or JS error )
    else {
      console.error('A TRUST-PLACE occured: \n =>', errorResponse);
    }

  }
}
