import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { AuthService } from '../services/auth-service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private router: Router, private authServ: AuthService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authServ.AuthenticatedUser.pipe(map((user: any) => {
      if (user) {

        const allowedRoles = route?.data?.['allowerRoles']

        // no particular role required
        if (allowedRoles.length === 0) {
          return true;
        }

        if (allowedRoles.length > 0) {
          return allowedRoles.includes(user.role);
        }

      }

      // user is not authenticated
      this.router.navigate(['login']);
      return false;

    }))
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.authServ.AuthenticatedUser.pipe(map((user: any) => {
      if (user) {

        const allowedRoles = route?.data?.['allowerRoles']

        // no particular role required
        if (allowedRoles.length === 0) {
          return true;
        }

        if (allowedRoles.length > 0) {
          return allowedRoles.includes(user.role);
        }

      }

      // user is not authenticated
      this.router.navigate(['login']);
      return false;

    }))
  }

}
