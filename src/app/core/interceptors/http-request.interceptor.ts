import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth-service/auth.service';
import { tap } from 'rxjs/operators';
import { GlobalErrorHandlerService } from '../services/global-error-handler/global-error-handler.service';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  constructor(private authServ: AuthService, private globalErrorServ: GlobalErrorHandlerService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // get auth token 
    const authToken = this.authServ.getStoredUserToken();
    if (authToken) {
      // token injection 
      request = request.clone({ setHeaders: { Authorozation: authToken } });
    }
    return next.handle(request).pipe(tap({
      next: (event) => {
        // handling error
      }, error: (errorResponse) => {
        // handling error
        this.globalErrorServ.handleError(errorResponse);
      }
    }));
  }
}
